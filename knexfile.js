require('./config');
var db_config = {
  client: 'pg',
  connection: {
    host: process.env.KNEX_HOST,
    user: process.env.KNEX_USER,
    password: process.env.KNEX_PASSWORD,
    database: process.env.KNEX_DATABASE,
  },
  migrations: {
    directory: "src/db/migrations"
  }
}

module.exports = db_config;

