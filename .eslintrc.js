module.exports = {
  "parserOptions":{
    "ecmaVersion": 6,
    "sourceType": 'module',
  },
  "env": {
    "node": true,
  },
  "extends": "eslint:recommended",
  "rules": {
    "semi": 2,
    "indent": ["error", 2],
    "comma-dangle": [2, 'always-multiline'],
    "no-console": 'warn',
    "no-unused-vars": ["error", {"argsIgnorePattern": "[iI]gnore"}], 
  }
};
