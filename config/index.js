var path = require('path');
require('dotenv').config();

module.exports = {
  port: 3000,
  version: 1.2,
};
