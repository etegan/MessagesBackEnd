import knex from './knex.js';
var bookshelf = require('bookshelf')(knex);
bookshelf.plugin('registry');

export default bookshelf;
