import app from '../app.js';
import winston from 'winston';


app.server.listen(3000, () => {
  winston.info('Listening on port 3000');
});

