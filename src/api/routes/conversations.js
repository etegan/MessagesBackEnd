import Conversation from '../models/conversation.js';
import Message from '../models/message.js';
import winston from 'winston';

module.exports = function(router, io){
  router.route('/:id/users')
    .get((req, res) => {
      Conversation.where({id: req.params.id}).fetch({withRelated: ['users']})
        .then(conversation => {
          res.json(conversation);
        });
    });
  router.route('/:id/messages')
    .post((req, res) => {
      var message = {
        user_id: req.body.user_id,
        message: req.body.message,
        conversation_id: req.params.id,
      };
      new Message(message).save()
        .then(model => {
          return model.fetch();
        })
        .then(result => {
          var message = result.toJSON();
          var s = `conv_${req.params.id}_user_${message.user_id}`;
          io.to(s).emit('message', message);
          res.json({
            message: message,
          });
        }).catch(err => {
          winston.error(err);
        });
    });
};
