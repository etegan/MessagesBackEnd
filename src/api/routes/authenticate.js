import User from '../models/user.js';
import winston from 'winston';



module.exports = function(router, io){
  router.route('/')
    .post((req, res) => {
      if (req.user){
        User.where({id: req.user.id}).fetch()
          .then(result => {
            var user = result.toJSON();
            io.aliases[`user_${user.id}`] = req.body.socket;
            var resp = {
              id: user.id,
              username: user.username,
              email: user.email,
            };     
            res.json(resp);
          })
          .catch(err => {
            winston.error(err);
            res.status(401).end();
          });

      }
      else{
        res.status(401).end();
      }
    });
};
