import bcrypt from 'bcrypt';
import User from '../models/user.js';
import jwt from 'jsonwebtoken';
import winston from 'winston';

const saltRounds = 10;

module.exports = function(router, io){
  router.route('/')
    .post((req, res) => {
      bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
        if(hash){
          var user = {
            username: req.body.username,
            email: req.body.email,
            passhash: hash,
          };
          new User(user).save()
            .then(model => {
              var user = model.toJSON();
              io.aliases[`user_${user.id}`] = req.body.socket;
              var resp = {
                user: {
                  username: model.username,
                  id: model.id,
                  token: jwt.sign({id: user.id}, process.env.JWT_SECRET),
                },
              };
              res.json(resp);
            })
            .catch(err =>{
              if(err.constraint == 'users_email_unique'){
                res.status(409).json({
                  error: 'Email alredy exists!',
                });
              }else{
                res.status(500).end();
              }
            });
        }else{
          winston.log(err);
          res.status(500).end();
        }
      });
    });
};
