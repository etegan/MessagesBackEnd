import express from 'express';
var routes = require('require-dir')();
module.exports = function(app, io){ 
  Object.keys(routes).forEach((route) => {
    var router = express.Router();
    require('./'+route)(router, io);
    app.use('/' + route, router);
  });
};
