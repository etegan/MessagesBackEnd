import User from '../models/user.js';
import Promise from 'bluebird';
import Conversation from '../models/conversation.js';
import multer from 'multer';
import crypto from 'crypto';
import path from 'path';
import authorize from '../middleware/authorize.js';
import mkdirp from 'mkdirp';
import winston from 'winston';
import gm from 'gm';


var storage = multer.diskStorage({
  destination: function(req, file, cb){
    var dir = path.resolve(process.env.NODE_PATH, 'uploads', 'users', req.user.id.toString(), 'avatar');
    mkdirp(dir, err => {
      if (err) winston.error(err);
      cb(null, dir);
    });
  },
  filename: function(req, file, cb){
    crypto.randomBytes(16, function(err, raw){
      if (err) cb(err);
      const file_name = raw.toString('hex') + path.extname(file.originalname);
      cb(null, file_name);
    });
  },
});


var upload = multer({storage: storage});

module.exports = function(router, io){
  router.route('/')
    .post((req, res) => {
      var user = {
        name: req.body.name,
        email: req.body.email,
      };
      new User(user).save()
        .then(user => {
          return user.fetch();
        })
        .then(user => {
          res.json({
            message: "created user",
            user: user,
          });
        });
    });
  router.route('/:id/conversations')
    .get((req, res) => {
      User.where({id: req.params.id}).fetch({withRelated: [
        {'conversations.users': (qb) => {qb.column('username', 'id', 'email', 'avatar');}}, 'conversations.messages']})
        .then(result => {
          var conversations = result.toJSON().conversations;
          const alias = io.aliases[`user_${req.params.id}`];
          var socket = io.sockets.connected[alias];
          if(socket){
            conversations.forEach(conversation =>{
              var recipients = conversation.users.filter(user => {
                return user.id != req.params.id;
              });
              recipients.forEach(recipient => {
                socket.join(`conv_${conversation.id}_user_${recipient.id}`);
              });
            });
          }
          res.json(conversations);
        });
    })
    .post((req, res) => {
      if(req.user.id == req.params.id){
        var getRecipient = User.where({email: req.body.recipient}).fetch();
        var createConversation = new Conversation().save();

        Promise.join(getRecipient, createConversation, (recipient, conversation) => {

          conversation.users().attach([req.params.id, recipient.id])
            .then(() => {
              return conversation.fetch({withRelated: [
                {'users': (qb) => {qb.column('username', 'id', 'email', 'avatar');}}, 'messages']});
            })
            .then(conversation => {
              console.log(io.aliases);
              const alias  = io.aliases[`user_${recipient.id}`];
              const socket = io.sockets.connected[alias];  
              socket.emit('conversation_created', conversation.toJSON());
              res.json(conversation);
            });
        });
      }else{
        res.status(401).end();
      }
    });
  router.route('/:id/avatar')
    .post(authorize, upload.single('avatar'), (req, res) => {
      const avatar_path = `/users/${req.user.id}/avatar/${req.file.filename}`;
      const avatar_full_path = path.resolve(process.env.NODE_PATH, 'uploads', 'users', `${req.user.id}/avatar/${req.file.filename}` );

      gm(avatar_full_path).gravity('north').thumb('100', '100', avatar_full_path, err => {
        if (err) winston.error(err);
        new User({id: req.params.id}).save({avatar: avatar_path}, {patch: true})
          .then(model =>{
            var user = model.toJSON();
            res.json({
              avatar: user.avatar,
            });
          })
          .catch(err => {
            winston.error(err);
          });
      });
    });
};
