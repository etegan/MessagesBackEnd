import User from '../models/user.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import winston from 'winston';


module.exports = function(router, io){
  router.route('/')
    .post((req, res) => {
      User.where({email: req.body.email}).fetch()
        .then(result => {
          var user = result.toJSON();
          bcrypt.compare(req.body.password, user.passhash, function(err, result){
            if (result){
              console.log(req.body.socket);
              console.log(`user_${user.id}`);
              io.aliases[`user_${user.id}`] = req.body.socket;
              var resp = {
                id: user.id,
                username: user.username,
                email: user.email,
                token: jwt.sign({id: user.id}, process.env.JWT_SECRET),
              };     
              res.json(resp);
            }else{
              winston.error(err);
              res.status(401).end();
            }
          });
        })
        .catch(err => {
          winston.error(err);
          res.status(401).end();
        });
    });
};
