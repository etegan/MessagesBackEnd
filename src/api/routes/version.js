import config from '../../config';

module.exports = function(router){
  router.route('/')
    .get((req, res) => {
      res.json({
        version: config.version,
      });
    });
};
