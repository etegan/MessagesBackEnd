export default function(req, res, next){
  if(req.user && (req.params.user_id == req.user.id || req.params.id == req.user.id)){
    next();
  }else{
    res.status(401).end();
  }
}
