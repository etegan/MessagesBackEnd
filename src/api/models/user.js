import bookshelf from '../../initializers/bookshelf.js';

var User = bookshelf.model('User', {
  tableName: 'users',
  conversations: function(){
    return this.belongsToMany('Conversation');
  },
});

export default User;
