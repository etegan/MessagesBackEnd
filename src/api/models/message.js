import bookshelf from '../../initializers/bookshelf.js';

var Message = bookshelf.model('Message', {
  tableName: 'messages',
  conversation: function(){
    return this.belongsTo('conversation');
  },
});

export default Message;
