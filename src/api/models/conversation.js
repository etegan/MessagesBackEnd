import bookshelf from '../../initializers/bookshelf.js';

var Conversation = bookshelf.model('Conversation', {
  tableName: 'conversations',
  messages: function(){
    return this.hasMany('Message');
  },
  users: function(){
    return this.belongsToMany('User');
  },
});

export default Conversation;
