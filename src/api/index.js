import express from 'express';

module.exports = function (io){
  var api = express();
  require('./routes')(api, io);
  return api;
};
