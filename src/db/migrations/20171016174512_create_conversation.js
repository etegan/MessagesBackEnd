
exports.up = function(knex, PromiseIgnored) {
  return knex.schema.createTable('conversations', (table) => {
    table.increments('id').primary();
    table.integer('initiator_id').unsigned().references('id').inTable('users');
    table.integer('participant_id').unsigned().references('id').inTable('users');
  });  
};

exports.down = function(knex, PromiseIgnored) {
  return knex.schema.dropTable('conversations');
};
