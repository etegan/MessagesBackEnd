
exports.up = function(knex, PromiseIgnored) {
  return knex.schema.table('conversations', (table) =>{
    table.dropColumn('initiator_id');
    table.dropColumn('participant_id');
  });  
};

exports.down = function(knex, PromiseIgnored) {
  return knex.schema.table('conversation', (table) => {
    table.integer('initiator_id').unsigned().references('id').inTable('users');
    table.integer('participant_id').unsigned().references('id').inTable('users');
  });  
};
