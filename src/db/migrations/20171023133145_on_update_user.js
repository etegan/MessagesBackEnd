
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('messages', function(table){
      table.dropForeign('user_id');
      table.dropForeign('conversation_id');
    }),
    knex.schema.table('messages', function(table){
      table.foreign('user_id').references('id').inTable('users').onDelete('cascade');
      table.foreign('conversation_id').references('id').inTable('conversations').onDelete('cascade');
    }),
    knex.schema.table('conversations_users', function(table){
      table.dropForeign('user_id');
      table.dropForeign('conversation_id');
    }),
    knex.schema.table('conversations_users', function(table){
      table.foreign('user_id').references('id').inTable('users').onDelete('cascade');
      table.foreign('conversation_id').references('id').inTable('conversations').onDelete('cascade');
    }),
  ]);  
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('messages', function(table){
      table.dropForeign('user_id');
      table.dropForeign('conversation_id');
    }),
    knex.schema.table('messages', function(table){
      table.foreign('user_id').references('id').inTable('users').onDelete('no action');
      table.foreign('conversation_id').references('id').inTable('conversations').onDelete('no action');
    }),
    knex.schema.table('conversations_users', function(table){
      table.dropForeign('user_id');
      table.dropForeign('conversation_id');
    }),
    knex.schema.table('conversations_users', function(table){
      table.foreign('user_id').references('id').inTable('users').onDelete('no action');
      table.foreign('conversation_id').references('id').inTable('conversations').onDelete('no action');
    }),
  ]);
};
