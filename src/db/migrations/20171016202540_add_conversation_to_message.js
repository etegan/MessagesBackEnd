
exports.up = function(knex, PromiseIgnored) {
  return knex.schema.table('messages', function(table){
    table.integer('conversation_id').unsigned().references('id').inTable('conversations');
    table.timestamps(true, true);
  }); 
};

exports.down = function(knex, PromiseIgnored) {
  return knex.schema.table('messages', function(table){
    table.dropTimestamps();
    table.dropColumn('conversation_id');
  });
};
