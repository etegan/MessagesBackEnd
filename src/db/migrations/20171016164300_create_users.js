
exports.up = function(knex, PromiseIgnored) {
  return knex.schema.createTable('users', function(table){
    table.increments('id').primary();
    table.string('email').unique();
    table.string('name');
  });  
};

exports.down = function(knex, PromiseIgnored) {
  return knex.schema.dropTable('users');
};
