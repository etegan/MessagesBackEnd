
exports.up = function(knex, PromiseIgnored) {
  return knex.schema.createTable('conversations_users', table => {
    table.integer('user_id').unsigned().references('id').inTable('users');
    table.integer('conversation_id').unsigned().references('id').inTable('conversations');
  });  
};

exports.down = function(knex, PromiseIgnored) {
  return knex.schema.dropTable('conversations_users'); 
};
