
exports.up = function(knex, PromiseIgnored) {
  return knex.schema.createTable('messages', (table) => {
    table.increments('id').primary();
    table.integer('user_id').unsigned().references('id').inTable('users');
    table.text('message');
  });  
};

exports.down = function(knex, PromiseIgnored) {
  return knex.schema.dropTable('messages');
};
