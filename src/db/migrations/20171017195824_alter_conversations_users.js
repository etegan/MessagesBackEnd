
exports.up = function(knex, PromiseIgnored) {
  return knex.schema.table('conversations_users', table => {
    table.primary(['user_id', 'conversation_id']);
  });  
};

exports.down = function(knex, PromiseIgnored) {
  return knex.schema.table('conversations_users', table => {
    table.dropColumn('user_id');
    table.dropColumn('conversation_id');
    table.integer('user_id').unsigned().references('id').inTable('users');
    table.integer('conversation_id').unsigned().references('id').inTable('conversations');
  });
};
