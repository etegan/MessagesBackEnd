
exports.up = function(knex, PromiseIgnored) {
  return knex.schema.table('users', table => {
    table.renameColumn('name', 'username');
    table.string('passhash');
  });  
};

exports.down = function(knex, PromiseIgnored) {
  return knex.schema.table('users', table => {
    table.renameColumn('username', 'name');
    table.dropColumn('passhash');
  });
};
