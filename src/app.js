import morgan from 'morgan';
import cors from 'cors';
import express from 'express';
import bodyParser from 'body-parser';
import jwt from 'express-jwt';
import http from 'http';
import path from 'path';



var app = express();
app.server = http.createServer(app);
var io = require('socket.io')(app.server);
io.aliases = {};
var api = require('./api')(io);

io.on('connection', function(socket){
  socket.emit('socket_id', socket.id);
});

app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(jwt({
  secret: process.env.JWT_SECRET,
  credentialsRequired: false,
  getToken: function fromHeaderOrQuerystring (req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
      return req.headers.authorization.split(' ')[1];
    } else if (req.query && req.query.token) {
      return req.query.token;
    }
    return null;
  },
}));
app.use(cors());
app.use(api);
app.use(express.static(path.resolve(__dirname, '../uploads')));
app.get('/', (req, res) => {
  res.send('Hello world');
});


export default app;
